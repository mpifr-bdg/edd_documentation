Deploying the EDD
=================

Requirements
------------
The EDD requires certain configurations respectively features available on a
cluster:

* Usermangement with shared home directories and unifrom logins on all nodes
* Passwordless (Key-based or Kerbero based o agent-based) ssh login to all
  nodes
* Root (or sudo) access for the deploying person
* Docker installed
* python3.8 or later on the nodes

* All nodes need to be accessible by their hostname on all nodes, IPs are not
  enough. This can achieved by DNS (preferred) or managing ``/etc/hosts``
* All nodes need to have access to a shared file system for data.





Basic Steps
-----------
#. A inventory and provision descriptions needs to be created. The provisioning repository for `SKAMPI <https://gitlab.mpcdf.mpg.de/mpifr-bdg/edd_provisioning_skampi>`_ telescope may be used as a template.
#. Ensure that the configuraton of the bare metJal machines is sufficient: ``ansible-playbook -i MY_SITE mysite_config.yml --tags=baremetal``
#. Download the baseimages from docker.io : ``ansible-playbook -i MY_SITE mysite_config.yml --tags=update-baseimages``
#. Build the EDD : ``ansible-playbook -i MY_SITE mysite_config.yml --tags=buildall``
#. Deploy : ``ansible-playbook -i MY_SITE mysite_config.yml``

