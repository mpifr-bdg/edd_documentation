.. _provisioning:

The Provisioning System
==============================================================================

The provisioning system serves two purposes:

   1. Starting/Stopping a group of pipelines as needed for the observation
   2. Deploy the core services on the backend computing system.

Both are achieved by executing corresponding ansible playbooks. While for
provisioning this an also be done on the commandline, the intention is that the
master controller loads a provision description into the system on a provision
katcp request.


Provision Descriptions
------------------------------------------------------------------
To provision the EDD for an observation **foo** two configuration files are
needed, the ansible-playbook **foo.yml** associating the pipeline roles to
individual machines and the basic configuration of the pipelines **foo.json**.
The basic configuration in articular  needs to connect the input output data
streams of the individual components.

The master controller automatically associates ansible-playbooks (.yml) and
basic con figurations with the same name. However, if needed both can be
specified individually in the request - sending '"foo.yml;bar.json"' instead of
'foo' will load the playbook foo with the configuration bar. Note that the
quotation marks are required here.


An example for an ansible playbook foo.yml may look as e.g.

.. code-block:: yaml

   ---
   - hosts: gpu_server[0]
     roles:
       - role: gated_full_stokes_spectrometer
         container_env: "EDD_ALLOWED_NUMA_NODES=0"
       - role: pulsar_pipeline
         container_name: pulsar_pipeline1
         data_base_path: "/beegfsEDD"
         container_env: "EDD_ALLOWED_NUMA_NODES=1"

   - hosts: gpu_server[1]
     roles:
       - role: fits_interface
         container_env: "EDD_ALLOWED_NUMA_NODES=0"
       - role: dig_pack_controller
         container_name: dig_pack_controller
         device: faraday_room_packetizer

Here Two roles are associated to the first two available gpu servers on the
site. On server 0, a pulsar_pipeline and a spectrometer will be running, on
server 1 a fits_interface and a dig_pack controller.

For larger setups running the same pipeline on multiple nodes with differnt
pipeline names can be automatically handled: 

.. code-block:: yaml

   ---
   - hosts: gpu_server[1:6]
     roles:
       - role: pulsar_pipeline
         container_name: pulsar_pipeline_{{ groups['gpu_server'].index(inventory_hostname)*2 - 1}}"
         container_env: "EDD_ALLOWED_NUMA_NODES=0"
       - role: pulsar_pipeline
         container_name: pulsar_pipeline_{{ groups['gpu_server'].index(inventory_hostname)*2 }}"
         container_env: "EDD_ALLOWED_NUMA_NODES=1"


This will start 10 pulsar pipelines numbered from 0 to 9 on the gpu servers
1-6. Note that skipping gpu_server[0] here has to be accounted for in the
numbering mechanism.


Pipeline ID
"""""""""""
Every pipeline needs an unique pipeline id. The pipeline_id is sued as name of
the docker container and also to identify the pipeline in the configuration
file.

Every pipeline has a specific default name. This works fine as long as there is
only one pipeline of this type configured in the system. If there is more than
one pipeline of the same type, the default container name has to be specified
using the ``container_name`` option.

.. todo::
   Rename container_name to pipeline_id


Numa Node Selection
"""""""""""""""""""
Technically, every container is started with access to the full system. Access
tu numa nodes is regulated programatically inside the pipeline server using the environment
variable ``EDD_ALLOWED_NUMA_NODES``. The server has to guarantee that
subprocesses are run only on the corresponding numa node. Using the
``container_env:`` option, also additional environment variables may be set to
the container.


User group and Quota
""""""""""""""""""""
The pipeline container run with the uid / gid specified in the variables
``edd_user`` and ``edd_group`` which default to root. This can be changed on a
per product basis in the provision description by overriding the default
variable, e.g.:

.. code-block:: yaml

  ---
  - hosts: gpu_server[1]
    vars:
      edd_group: 50000
    roles:
      - role: pulsar_pipeline
      - role: gated_full_stokes_spectrometer

  - hosts: gpu_server[0]
    roles:
      - role: dig_pack_controller
        edd_group: 50001
      - role: fits_interface
        edd_group: 50002

In this example all pipelines on ``gpu_node[1]`` will run with gid 50000,
whereas the group for the pipelines on gpu_server[0] are choosen individually.

Note that in order to use named groups / user instead of the uid/gid, the users
have to be added to the ansible interface.





Data Directory
""""""""""""""
In every container the folder /mnt inside the container maps a folder
``data_base_path/pipeline_id`` on the host. The default data_base_path of the
site can be overwritten using the ``data_base_path`` option as e.g. for the
pulsar_pipeline1 in the example.

In case multiple pipelines should share a data path, the full data path for the
role can be
set using ``base_path``.




Controlled devices
""""""""""""""""""
Information about all hardware is stored in the inventory. Association of hardware
devices with hardware controlling pipelines is thus done also on level of the
ansible playbook. By this all information on IP adresses etc. is kept in on
place. The device to be controlled is associated with its
controller using the ``device`` keyword.

By this, the site inventory can in principle be managed dynamically using
connection to an external data base at the site.



Basic configuration
"""""""""""""""""""

The basic configuration of a provision discription is applied to the products
by the amster controller and in particular specifies the input data streams and
the dependencies of the products. For the example above it may e.g. look like:

.. code-block:: json

  {
    "products":
    [{
          "id":"dig_pack_controller",
          "bit_depth" : 8,
          "sampling_rate": 4000000000.0,
          "predecimation_factor" : 4,
          "flip_spectrum": true,
          "output_data_streams": {
              "polarization_0" : {
                  "format": "MPIFR_EDD_Packetizer:1",
                  "ip": "225.0.0.110+3",
                  "port": "7148" },
               "polarization_1" : {
                  "format": "MPIFR_EDD_Packetizer:1",
                  "ip": "225.0.0.114+3",
                  "port": "7148" } }
      },
      {
        "id": "gated_stokes_spectrometer",
        "input_data_streams": {
            "polarization_0": {
                "source": "dig_pack_controller:polarization_0",
                "format": "MPIFR_EDD_Packetizer:1" },
             "polarization_1": {
                "source": "dig_pack_controller:polarization_1",
                "format": "MPIFR_EDD_Packetizer:1" } }
      },
      {
        "id": "pulsar_pipeline1",
         "input_data_streams": [ {
                "source": "dig_pack_controller:polarization_0",
                "central_freq": 1200 },
            {
                "source": "dig_pack_controller:polarization_1",
                "central_freq": 1200 } ]
     }],
     "_comment": "Not all products are shown here for brevity"
  }


The individual options may be overwritten after the provisioning step using the
configure / set commands of the pipeline respectively the master controller.


.. _pipeline_role:

Pipeline Roles
------------------------------------------------------------------
Every pipeline as e.g. the pulsar pipeline or the gated spectrometer is
implemented in a dedicated role that creates the pipeline on provisioning.
Common tasks are implemented in the `common` role that acts as an abstract
interface for pipelines. Due to this abstraction the provision code for
pipelines such as e.g. the gated spectrometer pipeline requires only writing
one ansible task.

common-Role
"""""""""""

The common role is used to abstract out common tasks of provisioning an EDD
pipeline, e.g.

  * It allows to build all pipelines in a standardized way, in particular using `quicbuild.sh` script and `ansible-playbook ... --tags=build`.
  * Selecting a free port on the host for katcp communication
  * Creating and mounting of a dedicated directory on the host for the pipeline
  * launching of sidecars to scrape sensor data to influx and redis
  * Creating dashboards in the monitoring database.
  * Storing log files of the containers after shutdown.


A specific pipeline can be based on the common role by implementing a task
including the common role and specifying certain variables,  e.g. for the gated
spectrometer:

.. code-block:: yaml

  - name: Gated Spectrometer
    include_role:
       name: common
    vars:
       image_name: edd_gated
       default_container_name: gated_spectrometer
       container_cmd: "/usr/bin/python /usr/local/lib/python2.7/dist-packages/mpikat-0.1-py2.7.egg/mpikat/effelsberg/edd/pipeline/GatedSpectrometerPipeline.py --host={{ edd_subnet }} --port={{ bind_port }}"
       dashboard_panels:
         - "pipeline_status.json"
         - "mkrecv_dualpol.json"
       measurement: "gated_spectrometer"
    tags:
      - always



Required variables:
  image_name
    name of the image to be created.

  default_container_name
    default name to be used for containers of this pipeline

  container_cmd:
    Command to be executed in the container.
    Inside the common role a free port is selected on the host machine and stored
    in the variable `bind_port` that should be used by the pipeline for
    communication. A inventory level variable `edd_subnet` is used to specify the
    subnet from which katcp commands should be accepted.



Optional variables:
  data_dir:
    The host directory mounted to `/mnt` inside the container. It defaults to `"{{ data_base_path }}/{{ container_name }}"`

  container_env:
    Additional environment variables set inside the container

  dashboard_template:
    The template used for creating a dashboard for the provisioned pipeline.
    Defaults to "common_dashboard.json"

  dashboard_panels:
     a list of panels which are added to the provisioned dashboard

  measurement:
    The name of the measurement to which sensor data is stored in the influx
    database.

  redis_sidecar:
    If True, a redis sidecar is launched


Container build
...............
The pipeline is build based on the dockerfile in
`PIPLEINENAME/templates/Dockerfile`. The dockerfile is processed as a jinja2
template. Inside the Dockerfile, all containers *should* use the following variable
pairs to access the repositories:

      - MPIKAT Repository: `"{{ mpikat_repository }}/{{ mpikat_branch }}"`
      - Provision repository: `"{{ provision_repository }}/{{ provision_branch }}"`

to allow global switching between tags.

The created image is named as specified by the variable `image_name`. The
version tag is taken from the variable `version_tag`. The default is *latest*,
but overridden in the inventory to ensure uniform tagging of components.

Additional files required by the docker context are copied to the build
directory if an `additional_build_files` variable is specified in the pipeline
role, e.g. (See: roles/edd_master_controller/tasks/main.yml):

.. code-block:: yaml

  - name: Create temporary data directory
    tempfile:
      state: directory
    register: additional_build_files
    check_mode: false

  - name: Copy ssh key
    copy:
      src: "{{ base_path }}/data/id_rsa"
      dest: "{{ additional_build_files.path }}/id_rsa"
      mode: go-rwx



Launching of Side Cars
......................
By default, a side car is launched that monitors the pipeline sensors and
copies the values into the influx data base. If also a redis sidecar is
required, the variable `redis_sidecar: True` has to be set.







edd-base-Role
"""""""""""""
 In addition to the common role, the edd base role is used to build the edd
 base container. The edd base container container contains dependencies common
 to (many) pipelines, i.e. in particular:
  - Mellanox OFED support
  - psrdada
  - mkrecv, mksend
  - dependencies for mpikat

 The aim is here to reduce duplications in Docker files, branching of code
 versions and also compile times for pipeline containers.






Inventory
------------------------------------------------------------------
The inventory is a folder  containing the informations specific for any
individual deployment, as e.g. the Effelsberg and ska-proto folders of the
deployments at the Effelsberg Telescope or SKA-Prototype dish respectively. The
inventory folder contains a hosts file listing the ips/hostnames for the
deployment and their associated groups, and a subfolder defining group variables
for the individual hosts groups.

Groups are used here to attract out the individual
host names or IPs from the associated role in the setup.

Required groups
"""""""""""""""

The following groups need to be defined.

   * **gpu_server**, Host(s) that can be sued for GPU processing
   * **registry**, Hosts(s) that serve the docker registry.
   * **redis**, Host(s) that server the redis db.
   * **grafana**, Host(s) that serve the grafana monitor page.
   * **influx**, Host(s) on which an influxDB Server is installed.

Additional groups can be created for individual setups as needed. All hosts
need to be part of the group **all**.

The group variables file used for the **all** machines contains further
important site specifica and global constants.


Non-server networking devices
"""""""""""""""""""""""""""""

Also non server hardware are listed in a custom dictionary in the default
variables for the **all** group, e.g.:

.. code-block:: yaml

   npc_devices:
    skarab_00:
      interfaces:
        0:
          mac: 06:50:02:09:2a:01
          ip: 10.10.1.61
        1:
          mac: 06:50:02:09:2a:02
          ip: 10.10.1.62
      control_port: 7147

    faraday_room_packetizer:
      control_ip: 134.104.73.132
      control_port: 7147

    focus_cabin_packetizer:
      control_ip: 134.104.70.65
      control_port: 7147


Devices with an interface section are added as static ip in the dhcp
configuration.


Global Site variables
"""""""""""""""""""""

These varaiables specify global settings for the site. Note that although
`ansible variable precedence`_
allows to override these settings on a per host basis, ideally these settings
are kept uniform over all hosts in the system to avoid confusion and possibly
bugs.

.. _ansible variable precedence: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable

Repository Settings
^^^^^^^^^^^^^^^^^^^

These settings specify the repositories used to build the EDD In particular
useful to be tweaked in a local custom mysite_dev inventory to develop  the
provisioning system.

  mpikat_repository, mpikat_branch
	Repository and branch used toc heck out mpikat

  provision_repository, provision_branch
	Repository and branch of the provisiioing system (roles and provision
	descriptions) used by the master controller

  edd_inventory_folder
	Inventory file / directory in this repository to be used.

  docker_runtime
	 Default docker runtime


Network configuration
^^^^^^^^^^^^^^^^^^^^^
Parameters of the network configuration of the site

  edd_subnet
	The subnet from which the katcp server accept control connections

  high_speed_data_subnet
	Subnet used for highspeed data connections

  edd_ansible_port
	Port used for the ssh conenctions of the master controller tot he ansible interface



File System Settings
^^^^^^^^^^^^^^^^^^^^
Settings of the shared file system / common file system layout of the nodes.

  data_base_path
	Data base path to be used for pipelines. The pipelines will create
	data_base_path/container_name as directory and see this internally as /mnt
	This can be overriden on per role basis by setting data_path variable
	for the role


Service variables
^^^^^^^^^^^^^^^^^
These variables are used to specify the host (and ports) on which a specific
service is running. Typically, the first available host in a service group is
used. Configuring the same role on multiple service machines thus automatically
provides a simple redundancy.

.. code-block:: yaml

  redis_storage: "{{ groups['redis'][0] }}"
  docker_registry: "{{ groups['registry'][0] }}"
  docker_registry_port: "5000"

  # ports range to search for a free port for aktcp servers
  edd_katcp_port_range_start: 7147
  edd_katcp_port_range_stop: 7189

  interface_host:  "{{ groups['interface'][0] }}"

  influx_host: "{{ groups['influx'][0] }}"
  influxdb_port: "8086"

  grafana_host: "{{ groups['grafana'][0] }}"
  grafana_port: "3000"


.. todo:: These variables should be set by default inside the role,w ith the option to be overriden on later levels.



Basic Configuring and Deployment on a new Cluster
------------------------------------------------------------------

Deploying the EDD on a new cluster should only include the following steps. Basic requirements are

 #. (Passwordless) ssh access to the individual nodes
 #. The executing user has permissions to control the docker daemon and grant the appropriate capabilities to the container

.. Warning:: This effctively means that the user is required to have root
	access to the system. However, the system is intended to run mostely as
	non-root user tp provide at least some minimum level of safety. Note also that
	there are basically no security features in the system so that any user with
	access to it has to be trusted to a very high degree. The complete system
	should be thus tucked away in a non-public network.

With these requirements few steps are necessary to deploy the EDD:

  * Writing a new inventory listing all hosts and specify the global configuraton settings
  * Create site specific playbook

   In the simplest case this can be empty except including the basic config.

	.. code-block:: yaml

		---
		- name: Include basic configuraton
		  import_playbook: basic_configuration/main.yml

   However, likely site specific services as interfaces and telescope meta data
   listener need to be deployed on specific hosts.

  * (Optionally) Additional bare metal configurations need to be applied. In
	particular access to the edd docker registry needs to be granted by installing
	the required certificates on the host. This can be done by executing:

	``$ ANSIBLE_CACHE_PLUGIN=memory ansible-playbook -i my_inventory my_site_config.yml --tags=baremetal``

  * The container of the core system can now be build using

	``$ ANSIBLE_CACHE_PLUGIN=memory ansible-playbook -i my_inventory my_site_config.yml --tags=build``

  * and afterwards the core system be deployed via

	``$ ANSIBLE_CACHE_PLUGIN=memory ansible-playbook -i my_inventory my_site_config.yml``

  * everything can also be done in one go:

	``$ ANSIBLE_CACHE_PLUGIN=memory ansible-playbook -i my_inventory my_site_config.yml --tags=baremetal,build,all``

  * Rebuilding the master controller can also be done separately:

	``$ ANSIBLE_CACHE_PLUGIN=memory ansible-playbook -i my_inventory my_site_config.yml --tags=build_master``


Additional playbooks are available to perform administrative tasks. They may be
included into site specific configurations. Also more bare metal configurations
can be included into the system. Note that the purpose of the separation of
build and baremetal tags and consequently multiple commands is to safe time on
repeated executions during development.


.. Note:: When the cluster is configured so that sudo is used for privilige
   escalation, use ``ansible-playbook -b- K ...``
   Note that if in such a configuration homes are located on a network file system, the ansible tmp directories may overlap as they default to $HOME/.ansible/tmp. They can be moved to a different location by specifying e.g.

   .. code-block::

      [defaults]
      local_tmp = /tmp/ansible_tmp_local
      remote_tmp = /tmp/ansible_tmp_remote

   int the ``ansible.cfg`` file.

