Debian Build
============

To ease installation of the EDD, dependencies and software is distributed in
binary form as Debian packages. These packages are not build to Debian quality
standards with respect to licensing, man pages, etc., depend on CUDA
and will likely build only on X86 architectures.

For every target build a Dockerfile in ``edd_provison/$BUILDVERSION/`` with the
``$BUIDLVERSION`` defined in the .buildtemplate exists. The provision_core GitLab
runner then has a manual build job to trigger the build of the build
environments. Ideally, the only has to be done once.

Every software build for the EDD, the repository has to be forked to the MPCDF.
This ensures the availability of the software in the required version. Inside
the fork, the Debian folder with the deb build settings is created, and a
``.gitlab-ci.yml`` file that imports the ``.build-template.yml`` is used to
automatically build the deb packages for the required versions of the build
matrix.


Update dependencies
+++++++++++++++++++

To update a dependency, we need to get the new upstream code into our
corresponding repository on GitLab and create a new deb version.

Get new upstream version
************************

Using upstream release tarballs
---------------------------------------------------
Example for psrchive

#.  Download new tarball, here ``psrchive-2024-09-15.tar.gz``
#.  Checkout / goto our repository

   .. code:: bash

      cd edd_dependencies/psrchive

#. Import new upstream version using git-buildpackage + enter version

   .. code:: bash

      $gbp import-orig ../psrchive-2024-09-15.tar.gz
      What is the upstream version? [15] 240915
      gbp:info: Importing '../psrchive-2024-09-15.tar.gz' to branch 'upstream'...
      gbp:info: Source package is psrchive
      gbp:info: Upstream version is 240915
      gbp:info: Merging to 'master'
      gbp:info: Successfully imported version 240915 of ../psrchive-2024-09-15.tar.gz





Using upstream git
--------------------------
Example using spead2

#. Checkout / goto our repository
#. Ensure you have remote upstream

    .. code:: bash

      $ git remote add upstream_origin https://github.com/ska-sa/spead2.git
#. Checkout upstream tags

   .. code:: bash

     $ git ls-remote --tags upstream_origin

#. Pull new version from upstream into our upstream branch

   .. code:: bash

     $ git co upstream
     $ git pull upstream_origin v4.3.2


Create new deb
**************

#. Check out main branch

   .. code:: bash

   $ git co master

#. Create new Debian version with corresponding version number and changelog entry

   .. code:: bash

      $ dch -v 240915+edd1


#. Test build locally

   .. code:: bash

      $ docker run --rm -v .:/src -it eddinfra0:5000/edd_build:cuda12.6.1_ubuntu24.04 bash
      $ cd /src
      # Install buiold dependencies
      $ apt build-dep -y .
      # Start the build
      $ debuild


#. If the build is working, we can  commit + push changes

   .. code:: bash
      # Update the Debian chagnelog to a released version
      $ dch -r
      $ git ci -m "New upstream release" debian/changelog
      $ git tag debianversion_tag
      $ git push --all --tags


The CICD system will create the new deb packages and push them to our
deb repository.





