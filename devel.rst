Development Notes
=================


Build Environments
------------------

The EDD software stack is build on the GitLab CI/CD system for several build
environments, i.e. a combination of ubuntu and cuda version such as e.g. ``cuda12.6.1_ubuntu22.04``.

For all environemnts, there is a dedicated build container
``eddinfra0:5000/edd_build:$BUILDENVIRONENT`` based on the corresponding cuda
devel container containing a minimum set of software to enablem builds in a
clean environment. The list of buildenvironments is fefined in the edd_provison
repository at two places,

#. ``.build_template.yml`` which is used by the CI/CD build matrix
#. ``roles/common/defaults/main.yml`` which is used by the ansible part of the build.

The cicd system of the provision core system has a manual job to ensure that
the EDD debian repositories are available for the individual build
environments.

