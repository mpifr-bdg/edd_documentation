Gated Spectrometer
==================

The gated spectrometer is a FFT based spectrometer / spectropolarimeter
with handling of fast switching noise diodes. It separates the data stream into two *gates* depending on
the status of the noise diode and calculates the output for both gates
independently. The data is separated by replacing heaps with non-matching noise
diode status with the average of the previous processing block.

There are two separate Gated Spectrometer Pipelines, the
:ref:`GatedSpectrometer <gated_spectrometer>` with independent (but time
aligned) output of the individual polarisations and the
:ref:`GatedFullStokesSpectrometer <gated_fullstokes_spectrometer>` with output of
the `Stokes parameters <https://en.wikipedia.org/wiki/Stokes_parameters>`_
calculated from both polarizations.

Dual Polarization Specrometer
------------------------------------------------------------------

.. _gated_spectrometer:
.. automodule:: mpikat.effelsberg.edd.pipeline.GatedSpectrometerPipeline



Full Stokes Specrometer
------------------------------------------------------------------

.. _gated_fullstokes_spectrometer:
.. automodule:: mpikat.effelsberg.edd.pipeline.GatedFullStokesSpectrometerPipeline
