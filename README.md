# EDD Documentation

This repository hosts the sphinx documentation of the EDD. The documentation
uses partially doc-strings from psrdada_cpp and mpikat.

The documentation is deployed to: http://mpifr-bdg.pages.mpcdf.de/edd_documentation/

