.. _available_pipelines:

Available Pipelines
------------------------------------------------------------------


.. toctree::

   digitizer_controller
   gated_spectrometer
   pulsar_pipeline
   hdf5_writer
