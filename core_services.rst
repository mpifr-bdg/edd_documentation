Core Services
=============

Master Controller
-----------------

.. todo:: Describe master controller a specialized pipelien here (+include mikat module doc)


Telescope State
---------------

.. todo:: Telescope state is exposed to components that need information. Describe mecahnism here.


Monitoring
----------

.. todo:: Dscibe monitoring system and injection of provisioined dashboards.


redis
"""""


influxdb
""""""""
Monitoring data from the pipelines is stored in an influx time series DB. The
DB has a retention policy as defined in the inventory.


grafana
"""""""
Grafana is sued to generate monitoring dashboards for the telescope meta data
and in particular the provisioned backend pipelines. The available dahboards
are modified acoring to the currently loaded provision description.

loki
""""
The loki service collects all logging output from the pipeline container. The
logs can be accessed using the grafana page.


