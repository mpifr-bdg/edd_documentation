Telescope Specific Components
=============================

Status Server
-------------
Some pipelines as e.g. the pulsar pipeline require additional information, e.g.
the source name or position of the telescope that cannot be provided by the
interface.  These telescope meta data is provided to the pipelines by the
EDDDataStore. The telescope specific status servers are responsible for
entering this information.


SCPI Interface
--------------

The SCPI interface translates the katcp interface of the master controller to
SCPI. For every katcp request of the master controller, a SCPI request is
implemented starting with a prefix ``EDD``, e.g.

  * ``EDD:PROVISION EXAMPLE``
  * ``EDD:CONFIGURE``
  * ``EDD:CAPTURE_START``
  * ...

Options of individual pipelines can be set by separating the keys of the config
dictionaries using colons mimicking the SCPI syntax, e.g.:

  * ``EDD:SET products:gated_stokes_spectrometer:fft_length 2048``
  * ``EDD:SET products:gated_stokes_spectrometer:naccumulate 256``
  * ``EDD:SET products:dig_pack_controller:noise_diode_pattern {\\\"percentage\\\":0.5,\\\"period\\\":0.01}``
  * ...

Note that sending json objects is requires extensive use of
escape characters to first escape the quotation marks and then escape the
escape characters, e.g. ``EDD:MEASUREMENTPREPARE {\\\"foo\\\":\\\"gna\\\"}``


FITS Writer Interface
---------------------

   .. todo:: Describe Fits writer interface



Blocking Client
---------------

   .. todo:: Describe Blocking Client (TCS USB Interface) here



