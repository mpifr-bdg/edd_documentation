Introduction
============

The EDD backend is comprised of individual services, each running inside a docker
container on a computing cluster. These services are in particular:

* Data processing pipelines
* Hardware controlling pipelines
* Data bases
* Data collecting side cars
* Monitoring website

as parts of the following building blocks of the EDD:


Pipelines
   Pipelines are realized as mpikat/katcp server that implement the
   :py:class:`EDDPipeline
   <mpikat.effelsberg.edd.edd_master_controller.EddMasterController>` interface.
   Inside the so realized common state-machine the individual pipelines map
   katcp commands to the state-machine of the controlled hardware, respectively software data processors.

   .. seealso:: :ref:`available_pipelines`, :ref:`pipeline_interface`


Provisioning
   The start any set of services is automatized using the
   `ansible <https://docs.ansible.com/ansible/latest/index.html>`_ IT
   automation tool. In particular the basic configuration of the system and
   the launch of a set of containers with corresponding configuration
   (provision description) is realized using ansible playbooks.

   .. seealso::
      :ref:`provisioning`


MasterController and Interface Server
   The master controller is the central continuously running pipeline of the
   EDD and the main user interface. It exposes katcp requests to
   provision/deprovision the system executing ansible as subprocess. Additional
   services are launched to translate telescope control commands such as e.g.
   SCPI commands in Effelsberg to the MasterController's katcp request. 


TelescopeState
   Pipelines may require information from the telescope - e.g. the name of the
   current source is required to load the appropriate models for pulsar timing.
   All information on the current state of the system is stored in a redis
   database and exposed to pipelines inside of mpikat.


Monitoring
   Data from the individual pipelines is scraped via specialized side-cars to
   the redis and/or an influx database. A grafana instance provides dashboards
   for the currently provisioned components.


