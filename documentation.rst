Writing Documentaton
====================

The EDD Documentation is written using the `sphinx
<https://www.sphinx-doc.org/>` documentation system. The sphinx system is used
in particular for many python packages. Documentation on sphinx and rest

Documentation should be ideally written close to the code implementation to
ease the synchronization between code development and documentation writing.
Inside the individual code repositories, the Full API documentation is
generated and references to this document using the intersphinx package.


Include Top Level Module Descriptions
------------------------------------------------------------------
The top level description of a component (module) can be included using:

.. code-block:: rst

   .. automodule:: mpikat.effelsberg.edd.edd_master_controller

yielding.

.. automodule:: mpikat.effelsberg.edd.edd_master_controller


This is the primary way to include the high level description of pipelines into
the main document.


Links to Pages
------------------------------------------------------------------

.. code-block:: rst

   * :std:doc:`mpikat:autoapi/mpikat/core/index`
   * :std:doc:`mpikat:autoapi/mpikat/effelsberg/edd/pipeline/GatedFullStokesSpectrometerPipeline/index`

will render as

* :std:doc:`mpikat:autoapi/mpikat/core/index`
* :std:doc:`mpikat:autoapi/mpikat/effelsberg/edd/pipeline/GatedFullStokesSpectrometerPipeline/index`


Links to API Documentation
------------------------------------------------------------------

.. code-block:: rst

   * Python

      * :py:class:`mpikat.core.scpi.Example`
      * :py:class:`mpikat.effelsberg.edd.edd_master_controller.EddMasterController`

   * C++

      * :cpp:class:`psrdada_cpp::RawBytes`
      * :cpp:class:`psrdada_cpp::effelsberg::edd::GatedStokesSpectrometer`

will render as

* Python

   * :py:class:`mpikat.core.scpi.Example`
   * :py:class:`mpikat.effelsberg.edd.edd_master_controller.EddMasterController`

* C++

   * :cpp:class:`psrdada_cpp::RawBytes`
   * :cpp:class:`psrdada_cpp::effelsberg::edd::GatedStokesSpectrometer`


Full Class Documentation
------------------------------------------------------------------

Full class documentation can be inserted using

.. code-block:: rst

   .. autoclass:: mpikat.effelsberg.edd.edd_master_controller.EddMasterController

yielding:

.. autoclass:: mpikat.effelsberg.edd.edd_master_controller.EddMasterController


