Monitoring
==========



Modify Existing Dashboards
--------------------------
The provisioned dashboards are added / removed automatically using he anisible
system, Any modifications will thus be lost on the next provisioning cycle.
To make changes permanent consider the following procedure:

 * make a copy of the dashboard
 * modify the copy
 * download the dashboard json
 * add the changes to the dashboard template in the provision repository

Adding New Dashboards
---------------------

Follow the analog procedure as in modifying an existing dashboard, but create a
new dashboard from scratch and add the dashboard as a new template to the role.

.. note:: The dashboard json contains a tag "id" set to some number. For the automatic creation / deletion to work, this id has to be  set to `null` in the template.




