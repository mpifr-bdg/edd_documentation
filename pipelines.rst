Pipelines
=========

From a **user's perspective** a pipeline is a component that performs certain tasks
during an observation. 
All pipelines are accessed by a common :ref:`interface <pipeline_interface>` of
katcp commands and thus have the same state machine. The state transitions and
details on using pipelines are described in the :ref:`interface section <pipeline_interface>`
The provided functionality and configuration options are
documented for every available pipeline:


.. toctree::
   :maxdepth: 2

   available_pipelines





.. -> see also master controller as special pipeine


From a **developer's perspective** a pipeline consists of multiple components:

   * A katcp pipeline server implementing the :ref:`pipeline interface <pipeline_interface>`.
   * A Docker container in which the pipeline server and possible data processing
     components runs.
   * Possibly monitoring components
   * Possibly one (or more) data processing program(s) or device(s)
   * A :ref:`pipeline role <pipeline_role>` in the provisioning system to launch and build the pipeline
     containers and inject the corresponding monitoring components into the
     monitoring system .

.. toctree::
   :maxdepth: 2
   :hidden:

   pipeline_interface

