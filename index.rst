##########################################
The Effelsberg Direct Digitization Backend
##########################################

Recent developments in high-speed networking and PCIe-mounted accelerator
cards have made possible new backend designs that provide vastly improved
flexibility and capability in signal processing for radio astronomy.
While direct digitization close to the receiver improves the quality of the
physical signal by reducing analog transmission and filtering losses, it also
enables the building of a backend based on commercial-off-the-shelf (COTS)
hardware that is able to exploit modern cloud computing paradigms.

The Effelsberg Direct Digitization (EDD) backend takes advantage of
these benefits.  The EDD backend orchestrates data processing on COTS computing
systems hosting GPUs as well as FPGA that can be rapidly and easily adapted to
a wide range of observing use cases, e.g. spectroscopy, polarimetry, pulsar
timing, etc.  The highly modular system is build of multiple micro services
with a unified interface to control its subsystems. This design abstracts the
telescope into well defined components suitable for deployment with  only
minimal customisation to individual radio observatories.

This document describes the design principels and current configuration of the
EDD version |version|, last updated |today|.


.. toctree::
   :hidden:

   Home <self>

.. toctree::
   :maxdepth: 2

   introduction
   pipelines
   core_services
   provisioning
   monitoring
   telescope_specific_components
   deployment
   debuild
   appendix

.. toctree::
   :caption: API Documentations
   :hidden:
   :maxdepth: 2

   mpikat <http://mpifr-bdg.pages.mpcdf.de/mpikat/>
   psrdada_cpp <http://mpifr-bdg.pages.mpcdf.de/psrdada_cpp/>


.. toctree::
   :caption: Code Repositories
   :hidden:
   :maxdepth: 2

   mpikat <https://gitlab.mpcdf.mpg.de/mpifr-bdg/mpikat>
   psrdada_cpp <https://gitlab.mpcdf.mpg.de/mpifr-bdg/psrdada_cpp/>
   edd_provision <https://gitlab.mpcdf.mpg.de/mpifr-bdg/edd_provisioning>
   katcp_monitor <https://gitlab.mpcdf.mpg.de/mpifr-bdg/katcp-monitor>
   mkrecv <https://gitlab.mpifr-bonn.mpg.de/mhein/mkrecv>
   mksend <https://gitlab.mpifr-bonn.mpg.de/mhein/mksend>


:ref:`genindex`

.. todolist::

