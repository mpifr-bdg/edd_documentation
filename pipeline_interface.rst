.. _pipeline_interface:

The Pipeline Interface
------------------------------------------------------------------

Any pipeline is required to implement the same state machine with transitions
initiated by corresponding katcp commands.

.. graphviz:: state_machine.dot
   :caption: State machine of the EDD pipelines. You can hover of the edges to visualize the commands initialting the transition. Intermediate states indicating the transition such ass configuring, capture-starting are not shown here. Error state can be reached from all states or if a state transition fails.


Pipeline States
+++++++++++++++

idle
  The pipeline software is running and awaiting settings or a configure
  command.

configured
  The pipeline has been successfully configured and is awaiting a capture_start
  command.

streaming
  The pipeline is streaming data and is not changing state for individual
  scans.

ready
  The pipeline is ready to be prepared for a measurement/scan.

set
  The pipeline has been successfully prepared for the scan

measuring
  The pipeline is currently measuring.

error
  An error occurred so that no further state transition can occur except
  deconfiguration of the pipeline to return to the idle state.

panic
  An error occurred due to deconfiguration. This should definitely not happen.
  It is possible to try deconfiguration again though.


Additional intermediate states configuring, capture_starting,
measurement_preparing, measurement_starting, measurement_stopping,
capture_stopping, deconfiguring are assumed during transitions between the above
listed state. In general, a timeout is used for the transition to complete so
that the intermediate state is changed to either the target state or error,
respectively panic for deconfiguring.


KatCP Commands
++++++++++++++

Every pipeline has to support the following commands to initiate the
corresponding state transitions:

?set "partial config"
    Updates the current configuration with the provided partial config.
    After set, it remains in state idle as only the config dictionary
    may have changed. A wrong config is rejected without changing state
    as state remains valid. Multiple set commands can be send to the
    pipeline.
?configure "partial config"
    Initiate state change from idle to configuring and configured (on success)
    or error (on fail). A partial config may be also provided here to allow
    configuration with one command only.
?capture_start
    Initiate state change from configured to streaming or ready (on success) or
    error (on fail). The streaming state indicates that no further changes to
    the state are expected and data is injected into the EDD.
?measurement_prepare "data"
    Initiate state change from ready to set or error
?measurement_start
    Initiate state change from set to running or error
?measurement_stop
    Initiate state change from running to set or error
?capture_stop
    Return to state idle by stopping the capture and deconfiguring the
    pipeline.
?deconfigure
    Restore state idle



Additional commands may be implemented for manual interaction with the
pipeline but may not interfere with the common state machine.

